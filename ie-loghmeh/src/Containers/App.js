import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import { Provider } from 'react-redux';
import store from '../Store';

import '../style/style.css';
import '../style/reset.css';
import '../style/home.css';
import '../Assets/my-icons-collection/font/flaticon.css';

import Home from '../Pages/Home';
import Restaurants from '../Pages/Restaurants';
import Login from '../Pages/Login';
import Register from '../Pages/Register';
import User from '../Pages/User';
import FourOFour from '../Pages/FourOFour';
import PrivateRoute from '../Components/auth/PrivateRoute'
//token
import setAuthToken from '../utils/setAuthToken';
import {logoutUser, setCurrentUser} from '../actions/authActions';
import jwt_decode from 'jwt-decode';

// Check for token
if (localStorage.jwtToken) { 
  // Set auth token header auth
  setAuthToken(localStorage.jwtToken);
  // Decode token and get user info and exp
  const decoded = jwt_decode(localStorage.jwtToken);
  // Set user and isAuthenticated
  store.dispatch(setCurrentUser(decoded));
  // Check for expired token
  const currentTime = Date.now() / 1000;
  if (decoded.exp < currentTime) {
    // Logout user 
    store.dispatch(logoutUser());
    // Redirect to login
    window.location.href = '/login';
  }
}

export default class App extends Component {
  render() {
    return (
      <Provider store = {store} >
        <Router>
          <div className='backgroundc font1'>
            <Switch>
              <PrivateRoute exact path="/" component={ Home } />
              <Route path="/login" component={ Login } />
              <Route path="/register" component={ Register } />
              <PrivateRoute path="/restaurant/:id" component={ Restaurants } />
              <PrivateRoute path="/user" component={ User } />
              <Route component={ FourOFour } />
            </Switch>
          </div>
        </Router>
      </Provider>
    )
  }
}
