import axios from '../utils/request';
import { GET_RESTAURANTS, GET_FOOD_PARTY, GET_ERRORS, LOAD_RESTAURANT } from '../utils/actionTypes';

export const getRestaurants = () => dispatch => {
  axios
    .get('/restaurants')
    .then(res =>
      dispatch({
        type: GET_RESTAURANTS,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: {"restaurants": "server error loading restaurants"}
      })
    );
};

export const homeSearch = (fields) => dispatch => {
  console.log('getting search results for ...')
  console.log(fields)
  axios
    .get('/restaurants?restaurant=fields.restaurantField&food=fields.foodField')
    .then(res =>
      console.log(res)
      // dispatch({
      //   type: GET_RESTAURANTS,
      //   payload: res.data
      // })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: {"restaurants": "server error loading restaurants"}
      })
    );
};

export const getFoodParty = () => dispatch => {
  axios
    .get('/food_party')
    .then(res =>
      dispatch({
        type: GET_FOOD_PARTY,
        payload: res.data
      })
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: {"foodParty": "server error loading foodParty"}
      })
    );
};

export const getRestaurantById = (id) => dispatch => {
  console.log("restaurant actions",id)
  axios
    .get('/restaurants/'+id)
    .then(res =>
      dispatch({
        type: LOAD_RESTAURANT,
        payload: res.data
      })
      // console.log(res.data)
    )
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: {"restaurat": "server error loading restaurant"}
      })
    );
};

