import axios from '../utils/request';
import { GET_ERRORS, SET_CURRENT_USER } from '../utils/actionTypes'; 
import setAuthToken from '../utils/setAuthToken';
import jwt_decode from 'jwt-decode';

//Register User
export const registerUser = (userData, history) => dispatch => {
  console.log("user data", userData);
  axios
    .post('/users/sign-up', userData)
    .then(res => {
      console.log(res);
      history.push('/login');
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: {"register": "data are not correct."}
      })
    );
};

export const loginUser = (userData, history) => dispatch => {
  console.log("user data", userData);
  axios
    .post('/login', userData)
    .then(res => {
      console.log("token:", res.data.token)
      // Save to localStorage 
      const { token } = res.data;
      // Set token to ls
      localStorage.setItem('jwtToken', token);
      // Set token to Auth header
      setAuthToken(token);
      // Decode token to get user data
      const decoded = jwt_decode(token);
      // Set current user
      dispatch(setCurrentUser(decoded));
      history.push('/');
    })
    .catch(err =>
      dispatch({
        type: GET_ERRORS,
        payload: {"login": "email or password is not correct."}
      })
    );
};

// Set logged in user
export const setCurrentUser = decoded => {
  return {
    type: SET_CURRENT_USER,
    payload: decoded
  };
};

// Log user out
export const logoutUser = (history) => dispatch => {
  // Remove token from localStorage
  localStorage.removeItem('jwtToken');
  // Remove auth header for future requests
  setAuthToken(false);
  // Set current user to {} which will set isAuthenticated to false
  dispatch(setCurrentUser({}));
  if(history !== undefined) {
      history.push('/login')
  }
};