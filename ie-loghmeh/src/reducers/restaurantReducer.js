import { GET_RESTAURANTS, GET_FOOD_PARTY, LOAD_RESTAURANT, ADD_TO_CART } from '../utils/actionTypes'; 

const initialState = {
  restaurants: [],
  foodParty: [],
  restaurant: {}
}
export default function(state = initialState, action) {
  switch(action.type) {
    case GET_RESTAURANTS:
      return {
        ...state,
        restaurants: action.payload
      };
    case GET_FOOD_PARTY:
      return {
        ...state,
        foodParty: action.payload
      };
    case LOAD_RESTAURANT:
      return {
        ...state,
        restaurant: action.payload
      };
    case ADD_TO_CART:
      console.log(action.payload)
      return {
        ...state,
        restaurant: {}
      };
    default:
      return state;
  }
}