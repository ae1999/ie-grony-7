import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { homeSearch } from '../../actions/restaurantActions';

class HomeHeader extends Component {
  constructor() {
    super();
    this.state = {
      foodField: '',
      restaurantField: ''
    }
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  onChange = e => this.setState({ [e.target.name]: e.target.value });
  onSubmit = e => {
    e.preventDefault();
    const searchFields = {
      foodField: this.state.foodField,
      restaurantField: this.state.restaurantField
    }
    this.props.homeSearch(searchFields);
  }
  render() {
    return (
      <div>
        <div className="app-background">
          <div className="behind-img">
            <img src={require("../../Assets/Cover Photo.jpg")} alt=""/>
          </div>
          <div className="center-things">
            <div className="loghme-icon">
              <img className="home-loghme-icon-img" src={require("../../Assets/LOGO.png")} alt=""/>
            </div>
            <div className="loghme-title">
              اولین و بزرگترین وب‌سایت سفارش آنلاین غذا در دانشگاه تهران
            </div>
          </div>
        </div>
            <div className="search-bar">
          <div className="outer-box">
            <form
              onSubmit={this.onSubmit}
              className="w-100 m-0 d-flex justify-content-center align-items-center">
              <div className="row w-100">
                <div className="food-name col">
                  <input
                    className="search-item"
                    name="foodField"
                    value={this.state.foodField}
                    onChange={this.onChange}
                    placeholder="نام غذا"/>
                </div>
                <div className="restaurant-name col">
                  <input 
                    className="search-item"
                    name="restaurantField"
                    value={this.state.restaurantField}
                    onChange={this.onChange}
                   placeholder="نام رستوران"/>
                </div>
                <div className="search-btn col">
                  <input className="search-item" type="submit" value="جست‌و‌جو"/>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    )
  }
}

HomeHeader.propTypes = {
  homeSearch: PropTypes.func.isRequired,
  restaurant: PropTypes.object.isRequired,
};

const mapStateToProps = (state) => ({
  restaurant: state.restaurant
});

export default connect(mapStateToProps, { homeSearch })(HomeHeader);
