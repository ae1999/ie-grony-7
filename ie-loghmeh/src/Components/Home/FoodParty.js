import React, { Component } from 'react';
import PropTypes from 'prop-types'

import CenterTitle from "../layout/CenterTitle";
import FoodPartyCart from '../../Components/Home/FoodPartyCart';

import { connect } from 'react-redux';
import { getFoodParty } from '../../actions/restaurantActions';

class FoodParty extends Component {
  componentDidMount() {
    this.props.getFoodParty();
    
  }
  render() {
    const pic1 = "https://static.snapp-food.com/200x201/cdn/43/39/8/vendor/5dc6c1040aa6b.jpg"
    const pic2 = "https://static.snapp-food.com/200x201/cdn/27/44/3/vendor/5d2f7096203c1.jpg"
    let menu = [];
    if(this.props.restaurant.foodParty !== undefined || this.props.restaurant.foodParty !== null) {
      menu = this.props.restaurant.foodParty.map((m, i) =>
        <FoodPartyCart 
          res={m.restaurantName} 
          res_id={m.restaurantId} 
          name={m.name} 
          price={m.price} 
          count={m.count}
          oldPrice={m.oldPrice} 
          star={m.popularity} 
          img={i%2 ? pic1 : pic2} 
          key={i} 
        />
      );
    }
    return (
      <div>
        <CenterTitle text="جشن غذا!"/>
        <div className="foodPartyTime alert alert-info d-flex justify-content-center" role="alert">
          زمان باقیمانده:
          ۲۹:۵۸
        </div>
        <div className='slider my-4'>
           {menu}
        </div>
      </div>
    )
  }
}

FoodParty.propTypes = {
  getFoodParty: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  restaurant: state.restaurant,
});

export default connect(mapStateToProps, { getFoodParty })(FoodParty);