import React from 'react';
import { Link } from 'react-router-dom';
import LOGO from '../../Assets/LOGO.png';
import {enToFaNumber} from "../../utils/utils";
import { connect } from 'react-redux';
import Modal from 'react-bootstrap/Modal';
import Cart from './Cart';

class Navbar extends React.Component {
	constructor(props) {
		super(props);
		this.handleShow = this.handleShow.bind(this);
		this.handleClose = this.handleClose.bind(this);
		this.logout = this.logout.bind(this);

		this.state = {
			show: false,
		};
  }
	handleClose() {
		this.setState({ show: false });
	}

	handleShow() {
		this.setState({ show: true });
	}

	logout() {
		localStorage.removeItem("jwtToken");
		window.location = "/login";
	}

	render() {
	return (
		<nav className="navbar navbar-light bg-light justify-content-between fix-top">
		<button className="navbar-brand ml-5 text-danger" onClick={this.logout} >خروج</button>
		<Link className="navbar-brand ml-5" to= "/user">حساب کاربری</Link>
		<div className="navbar-item">
			<div className="display-flex" onClick={this.handleShow}>
				<div className="flaticon-smart-cart" href="/"><i></i></div>
				<div className="cart-items">
					<span>
					{this.props.user.cart.items && <div className='mmm'>{enToFaNumber(this.props.user.cart.items.length)}</div>}
					</span>
				</div>
			</ div>	
		</div>
		
		<div className="my-2 my-lg-0 mr-4">
			<Link to="/"><img src={ LOGO } width="50" height="50" alt="" /></Link>
		</div>
			<Modal size="lg" show={this.state.show} onHide={this.handleClose}>
              <Cart/>
            </Modal>
		</nav>
	)
	}
}

const mapStateToProps = state => ({
  restaurant: state.restaurant,
	auth: state.auth,
	user: state.user
});

export default connect(mapStateToProps, {})(Navbar);