import React from 'react';
import copywrite from '../../Assets/my-icons-collection/copyright.png';

export default function Footer() {
  return (
    <footer className="footer">
      <img className="ml-2" src={ copywrite } width="20" height="20" alt=""/>
      <p>تمامی حقوق متعلق به لقمه است.</p>
    </footer>
  )
}