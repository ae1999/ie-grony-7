import React, { Component } from 'react';
import PropTypes from 'prop-types';

import starr from '../../Assets/my-icons-collection/star.png';

import { connect } from 'react-redux';
import {addToCart} from '../../actions/userActions';

class Cart extends Component {
  onClickAddToCart = e => {
    this.props.addToCart({
      foodName: this.props.name,
      restaurantId: this.props.resId
    })
    window.location.reload(false);
  }
  render() {
    return (
      <div className="d-inline-block float-right mb-2 mx-3 restaurant-cart shadow-sm p-2 mt-3 bg-white">
        <div className="mx-2">
          <img className="rounded" src={this.props.img} width="85" height="85" alt="" />
          <div className="my-2">
            <p className="d-inline fs">{this.props.name}</p>
            <div className="d-inline">
              <p className="d-inline mr-2 ">{parseInt(this.props.star*5)}</p>
              <img src={starr} width="15" height="15" alt=""  className="d-inline" />
            </div>
            <p className="mb-3">{this.props.price} تومان</p>
            <button onClick={this.onClickAddToCart} className="bill px-2 rounded">افزودن به سبد خرید</button>
          </div>
        </div>
      </div>
    )
  }
}

Cart.propTypes = {
  addToCart: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  restaurant: state.restaurant,
  user: state.user,
});

export default connect(mapStateToProps, { addToCart })(Cart);
