import React, { Component } from 'react'

export default class Header extends Component {
  render() {
    return (
      <div>
        <div className="my-container info-sec h12rem">
        </div>
          
        <div className="img-c">
          <img src={this.props.restaurantImg} className="res-image" width="180" height="180" alt="" />
          <h3 className="mt-4">{this.props.res}</h3>
        </div>
      </div>
    )
  }
}
