import React, { Component } from 'react'
import {enToFaNumber} from '../../utils/utils'

export default class BasketCart extends Component {
  render() {
    return (
      <div>
        <div className="border-bottom">
          <div>
            <div>
              <div className="d-inline">{this.props.name}</div>
              <div className="d-inline">
                <button value={this.props.name} onClick={this.props.minus} className="flaticon-minus icon-pos-v text-danger d-inline icon-pos-h-1"></button>
                <p className="d-inline icon-pos-h-1">{enToFaNumber(this.props.number)}</p>
                <button value={this.props.name} onClick={this.props.add} className="flaticon-plus icon-pos-v text-success d-inline icon-pos-h-2"></button>
              </div>
            </div>
          </div>
          <div className="price mt-1 tc">{enToFaNumber(this.props.price)} تومان</div>
        </div>
      </div>
    )
  }
}
