import * as React from "react";
import "../../style/orderdet.scss";
import PropTypes from "prop-types";

const enToFaNumber = text => {
  const arabicNumbers = ['۰', '۱', '۲', '۳', '۴', '۵', '۶', '۷', '۸', '۹'];
  return String(text).split('').map(c => parseInt(c) ? arabicNumbers[parseInt(c)] : c === '0' ? '۰' : c).join('');
}


export default class OrderDetail extends React.Component {
    constructor(props) {
        super(props);
        this.state = {}
    }


    render() {
        const items = this.props.cart.items;
        console.log(this.props.cart.restaurantName, items)
        return (
            <div className="wrapper">
                <div className="restaurant-title">
                    <span>
                        {items[0].foodName}
                    </span>
                </div>

                <div className="separator">

                </div>
                <div className="food-table">
                    <table className="table table-bordered">
                        <thead className="black white-text">
                        <tr> 
                            <th className="col-1">ردیف</th>
                            <th className="col-9">نام غذا</th>
                            <th className="col-1">تعداد</th>
                            <th className="col-1">قیمت</th>
                        </tr>
                        </thead>
                        <tbody>
                        {
                            this.props.cart && ! this.props.cart.empty  &&
                                this.props.cart.items.map((cartItem, index) => {
                                    return (
                                        <tr>
                                            <th scope="row">{enToFaNumber(index+1)}</th>
                                            <td>{cartItem.foodName}</td>
                                            <td>{enToFaNumber(cartItem.num)} 1</td>
                                            <td>{enToFaNumber(cartItem.price * cartItem.num)}</td>
                                        </tr>
                                    )
                                })
                        }
                        {
                            this.props.cart.items.size === 0 && (
                                <div className="text-center">
                                    خالی
                                </div>
                            )
                        }
                        </tbody>
                    </table>
                </div>

                <div className="price-wrapper">
                    <div className="price">
                        <span>
                            {enToFaNumber(this.props.cart.totalPrice)}
                            تومان
                        </span>
                        <span>
                             جمع کل:‌
                        </span>
                    </div>
                </div>
            </div>
        );
    }
}


OrderDetail.propTypes = {
    cart: PropTypes.shape({
        restaurant: PropTypes.shape({
            name: PropTypes.string
        }),
        size: PropTypes.number,
        cartItems: PropTypes.arrayOf({
            food: PropTypes.shape({
                name: PropTypes.string,
                price: PropTypes.number
            }),
            quantity: PropTypes.number

        }),
        totalPrice: PropTypes.number
    })
}