import React, { Component } from 'react';
import PropTypes from 'prop-types';

import Footer from "../Components/layout/Footer";
import Navbar from '../Components/layout/Navbar';
import Header from '../Components/Restaurant/Header';
import Cart from '../Components/Restaurant/Cart';
import BasketCart from '../Components/Restaurant/BasketCart';

import { connect } from 'react-redux';
import { getRestaurantById } from '../actions/restaurantActions';
import { getUserCart, finalizeOrder } from '../actions/userActions';

class Restaurants extends Component {
  constructor() {
    super();
    this.onClickFinalize = this.onClickFinalize.bind(this);
  }

  onClickFinalize() {
    this.props.finalizeOrder();
    window.location.reload(false);
  }
  
  onClickadd = e => {
    e.persist();
    console.log(e.target.value);
    // this.props.addOneFood(e);
  }
  onClickminus = e => {
    e.persist();
    console.log(e.target);
    // this.props.addOneFood(e);
  }
  componentDidMount() {
    const { id } = this.props.match.params;
    this.props.getRestaurantById(id);
    this.props.getUserCart();
  }
  render() {
    const {restaurant} = this.props.restaurant;
    let menu = [];
    let cartItem =[]
    if(restaurant !== null || restaurant !== undefined) {
      if(restaurant.menu !== undefined) {
        menu = restaurant.menu.map((m, i) =>
        <Cart 
          resId={this.props.match.params.id} 
          name={m.name} 
          price={m.price} 
          star={m.popularity} 
          img={m.image} 
          key={i} 
        />);
      }
    }
    const {cart} = this.props.user;
    if(cart !== null || cart !== undefined) {
      if(cart.items !== undefined) {
        cartItem = cart.items.map((m, i) =>
        <BasketCart key={i} name={m.foodName} price={m.price} number={m.num} add={this.onClickadd} minus={this.onClickminus} />);
      }
    }
    return (
      <div>
        <Navbar />
        <Header restaurantImg = {this.props.restaurant.restaurant.logo} res={this.props.restaurant.restaurant.name} />
          <div className="main-container row mb-3 pb-3">
            <div className="col-4 re">
              <div className="restaurant-cart shadow p-3 mt-5 bg-white rounded">
                <p className="border-bottom d-inline-block">سبد خرید</p>
                <div className="cart-box p-3 px-5">
                  {cartItem}
                </div>
                <p className="mt-2 mb-1">جمع کل: {this.props.user.cart.totalPrice} تومان</p>
                <button onClick={this.onClickFinalize} className="btn btn-b btn-align">تایید نهایی</button>
              </div>
            </div>
            <div className="col-8 bl">
            <h3 className="d-inline-block">منوی غذا</h3>
            <div className="b-r-d">
              <div className="mr-5">
                <div className="pt-3 px-5 d-inline-block m-auto mr-5">
                    {menu}
                </div>
              </div>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    )
  }
}

Restaurants.propTypes = {
  finalizeOrder: PropTypes.func.isRequired,
  getRestaurantById: PropTypes.func.isRequired,
  getUserCart: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  restaurant: state.restaurant,
  user: state.user
});

export default connect(mapStateToProps, { getRestaurantById, getUserCart, finalizeOrder })(Restaurants);