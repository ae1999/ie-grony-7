import React, { Component } from 'react';
import cc from "../Assets/my-icons-collection/copyright.png";
import LOGO from "../Assets/LOGO.png";
import {Link} from "react-router-dom";
import {withRouter} from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { registerUser } from '../actions/authActions';

class Register extends Component {
  constructor() {
    super();
    this.state = {
      name: '',
      fname: '',
      email: '',
      password: '',
      errors: {}
    }
    this.onChange = this.onChange.bind(this);
    this.onSubmit = this.onSubmit.bind(this);
  }
  // componentWillReceiveProps(nextProps) {
  //   if(nextProps.errors) {
  //     this.setState({errors: nextProps.errors})
  //   }
  //   console.log(this.state.errors);
  // }
  componentWillReceiveProps(nextProps) {
	  if (nextProps.auth.isAuthenticated) {
	    this.props.history.push('/');
	  }
	  if (nextProps.errors) {
			if(this.props.errors.error) {
				alert(this.props.errors.errors.register)
			}
	  }
	}
  componentDidMount() {
    if (this.props.auth.isAuthenticated) {
      this.props.history.push('/');
    }
  }
  onChange = e => this.setState({ [e.target.name]: e.target.value });
  onSubmit = e => {
    e.preventDefault();
    const newUser = {
      name: this.state.name,
      email: this.state.email,
      lastName: this.state.fname,
      password: this.state.password,
    }
    this.props.registerUser(newUser, this.props.history);
  };
  render() {

    return (
      <div className='bodyy'>
        <div className="bgg">
          <div className="img-c2 mt-6"><img src={LOGO} width="100" height="100" alt="" /></div>
          <h1 className="login">ثبت نام</h1>
          <div className="form-wrapper2">

            <form onSubmit={this.onSubmit} method="post" id="myform">
              
              <div>
                <input 
                  type="text"
                  name="name"
                  className= {'form-control mb-3'}
                  value={this.state.name}
                  onChange={this.onChange}
                  placeholder="نام"
                />
              </div>
              <div>
                <input 
                  type="text"
                  name="fname"
                  className= {'form-control mb-3'}
                  value={this.state.fname}
                  onChange={this.onChange}
                  placeholder="نام خانوادگی"
                />
              </div>
              <div>
                <input 
                  type="email"
                  name="email"
                  className= {'form-control mb-3'}
                  value={this.state.email}
                  onChange={this.onChange}
                  placeholder="ایمیل"
                />
              </div>
              <div>
                <input 
                  type="password"
                  name="password"
                  className= {'form-control mb-3'}
                  value={this.state.password}
                  onChange={this.onChange}
                  placeholder="رمز عبور"
                />
              </div>
              <div className="mml mt-4">
                <input type="submit" name="register" className="btn btn-credit mt-3 " value="ثبت نام" />
              </div>
            </form>



            <div className="mml">
            <Link to='./login' className="btn btn-credit mt-3">ورود</Link>
            </div>
          </div>
        </div>
        <footer className="footer2">
          <img src={cc} width="20" height="20" alt="" />
          <p className="d-inline-block p-0 m-0">تمامی حقوق متعلق به لقمه است.</p>
        </footer>
      </div>
    )
  }
}

Register.propTypes = {
  registerUser: PropTypes.func.isRequired,
  auth: PropTypes.object.isRequired,
  errors: PropTypes.object.isRequired
};

const mapStateToProps = (state) => ({
  auth: state.auth,
  errors: state.errors
});

export default connect(mapStateToProps, { registerUser })(withRouter(Register));