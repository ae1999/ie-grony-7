import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Footer from "../Components/layout/Footer"

import Navbar from '../Components/layout/Navbar';
import HomeHeader from '../Components/Home/HomeHeader';
import FoodParty from '../Components/Home/FoodParty';
import Restaurants from '../Components/Home/Restaurants';

import { connect } from 'react-redux';
import { getRestaurants, getFoodParty } from '../actions/restaurantActions';
import { getUserCart } from '../actions/userActions';

class Home extends Component {
  componentDidMount() {
    this.props.getRestaurants();
    this.props.getUserCart();
  }
  render() {
    return (
      <div>
        <Navbar />
        <HomeHeader />
        <FoodParty />
        <Restaurants restaurants = {this.props.restaurant.restaurants} />
        <Footer />
      </div>
    )
  }
}

Home.propTypes = {
  getRestaurants: PropTypes.func.isRequired,
  getFoodParty: PropTypes.func.isRequired,
  getUserCart: PropTypes.func.isRequired
}

const mapStateToProps = state => ({
  restaurant: state.restaurant,
});

export default connect(mapStateToProps, { getRestaurants, getFoodParty, getUserCart })(Home);